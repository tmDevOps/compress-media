FROM node:7.2-alpine
MAINTAINER mario@techmunchies.net

ENV \
  PKGNAME=graphicsmagick \
  PKGVER=1.3.25 \
  CBUILD=x86_64-alpine-linux-musl \
  CHOST=x86_64-alpine-linux-musl

COPY gulpfile.js package.json /

RUN set -ex && \
  apk add --update --no-cache --virtual .build-deps \
    autoconf \
    automake \
    bison \
    bzip2 \
    bzip2-dev \
    ca-certificates \
    coreutils \
    g++ \
    gcc \
    gdbm-dev \
    glib-dev \
    libc-dev \
    libffi-dev \
    libxml2-dev \
    libxslt-dev \
    lzip \
    make \
    nasm \
    wget && \
  apk add --update --no-cache \
    libbz2 \
    libjpeg-turbo-dev \
    libpng-dev \
    libtool \
    libgomp && \
  update-ca-certificates && \
  wget https://downloads.sourceforge.net/$PKGNAME/$PKGNAME/$PKGVER/GraphicsMagick-$PKGVER.tar.lz && \
  lzip -d -c GraphicsMagick-$PKGVER.tar.lz | tar -xvf - && \
  cd GraphicsMagick-$PKGVER && \
  ./configure \
    CC=gcc \
    --build=$CBUILD \
    --host=$CHOST \
    --prefix=/usr \
    --sysconfdir=/etc \
    --mandir=/usr/share/man \
    --infodir=/usr/share/info \
    --localstatedir=/var \
    --enable-shared \
    --disable-static \
    --with-modules \
    --with-threads \
    --with-gs-font-dir=/usr/share/fonts/Type1 \
    --with-quantum-depth=16 && \
  make && \
  make install && \
  cd / && \
  rm -rf GraphicsMagick-$PKGVER && \
  rm GraphicsMagick-$PKGVER.tar.lz && \
  npm install --silent && \
  apk del .build-deps

VOLUME /srv

WORKDIR /srv

CMD /node_modules/.bin/gulp
