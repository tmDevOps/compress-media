"use strict";

// Load gulp
var gulp = require('gulp');

// Loads gulp plugins using $.pluginname
var $ = require('gulp-load-plugins')();

// Parallelize Operations
var parallel = require('concurrent-transform');
var os       = require("os");

// Imagemin Plugins
var giflossy = require('imagemin-giflossy');
var mozjpeg  = require('imagemin-mozjpeg');
var pngquant = require('imagemin-pngquant');
var webp     = require('imagemin-webp');

// Load delete plugin
var del = require('del');

// Resize images
gulp.task('resize', () => {
    return gulp.src('/srv/_assets/images/**/*.{jpg,png}')
        .pipe(parallel(
            $.imageResize({
                width : 1000,
                crop : false,
                quality: 1,
                upscale : false
            }),
            os.cpus().length
        ))
        .pipe(gulp.dest('/srv/tmp/responsive'));
});

// Minify and Compress images (webp not supported yet)
gulp.task('images', ['resize'], () => {
    return gulp.src(['/srv/_assets/images/**/*.{gif,svg,tiff}', '/srv/tmp/responsive/**/*.{jpg,png}'])
        .pipe(parallel(
            $.imagemin([
                $.imagemin.gifsicle({interlaced: true}),
                $.imagemin.jpegtran({progressive: true}),
                $.imagemin.optipng(),
                $.imagemin.svgo({svgoPlugins: [{removeViewBox: false}]}),
                giflossy({optimizationLevel: 3, lossy: 80}),
                mozjpeg({quality: '70'}),
                pngquant({quality: '70-80'})
                //webp({quality: '50'}) // Not supported by latest gulp-imagemin, doesn't use correct ext
            ]),
            os.cpus().length
        ))
        .pipe(gulp.dest('/srv/assets/images'));
});

// Minify and Compress images -- outputting webp files
gulp.task('webp', ['images'], () => {
    return gulp.src(['/srv/_assets/images/**/*.{tiff}', '/srv/tmp/responsive/**/*.{jpg,png}'])
        .pipe(parallel(
            $.webp({quality: 50, alphaQuality: 50}),
            os.cpus().length
        ))
        .pipe(gulp.dest('/srv/assets/images'));
});

// Clean all temporary directories from build process
gulp.task('clean:temp', ['webp'], () => {
    return del([
        '/srv/tmp/**/**'
    ]);
});

// Clean uncompressed images
gulp.task('clean:images', ['clean:temp'], () => {
    return del([
        '/srv/_assets/images/**/*.{jpg,png,gif,svg,tiff}',
        '/srv/_assets/images/posts/**'
    ]);
});

// Default gulp task
gulp.task('default', ['images', 'webp', 'clean:temp', 'clean:images']);

// Use if you don't want uncompressed source images to be deleted
gulp.task('no-del-orig', ['images', 'webp', 'clean:temp']);
